import '../styles/globals.scss'
import '../node_modules/bootstrap/scss/bootstrap.scss';
import Layout from '../components/layout';

export default function MyApp({ Component, pageProps }) {
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  )
}
