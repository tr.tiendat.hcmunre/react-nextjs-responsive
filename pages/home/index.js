
import Pricing from '/components/pricing/index';
import Features from '../../components/features';

export default function Home() {
  return (
    <>
        <Features></Features>
        <Pricing></Pricing>
    </>
  )
}
