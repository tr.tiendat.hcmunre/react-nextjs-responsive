import React from 'react';
import styles from './styles.module.scss';
import group163 from './icons/group-163.svg';
import group164 from './icons/group-164.svg';
import group165 from './icons/group-165.svg';
import group166 from './icons/group-166.svg';
import Image from 'next/image';

const Features = () => {
  const features = [
    {
      id: 1,
      icons: group163,
      width: 76,
      height: 76,
      description:
        'Lorem ipsum dolor sit amet, consectetuer adipiscing Lorem ipsum dolor sit amet consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna ',
    },
    {
      id: 2,
      icons: group163,
      width: 76,
      height: 76,
      description:
        'Lorem ipsum dolor sit amet, consectetuer adipiscing Lorem ipsum dolor sit amet consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna ',
    },
    {
        id: 3,
        icons: group164,
        width: 76,
        height: 76,
        description:
          'Lorem ipsum dolor sit amet, consectetuer adipiscing Lorem ipsum dolor sit amet consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna ',
    },
    {
      id: 4,
      icons: group164,
      width: 76,
      height: 76,
      description:
        'Lorem ipsum dolor sit amet, consectetuer adipiscing Lorem ipsum dolor sit amet consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna ',
    },
    {
      id: 5,
      icons: group165,
      width: 76,
      height: 76,
      description:
        'Lorem ipsum dolor sit amet, consectetuer adipiscing Lorem ipsum dolor sit amet consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna ',
    },
    {
      id: 6,
      icons: group166,
      width: 76,
      height: 76,
      description:
        'Lorem ipsum dolor sit amet, consectetuer adipiscing Lorem ipsum dolor sit amet consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna ',
    }
  ];

  const FeatureRender = features.map((feature, index) => {
    return (
        <div className="col-md-4 col-lg-4" key={index}>
            <div className={`card border-0`}>
                <div className={`card-body ${styles.features__icon}`}>
                    <Image
                        className={``}
                        src={feature?.icons}
                        priority={true}
                        width={feature?.width}
                        height={feature?.height}
                        alt=""
                    ></Image>
                    <p className="card-text p-3">{feature?.description}</p>
                </div>
            </div>
        </div>
    );
  });


  return (
    <div className="row justify-content-evenly pt-3 pb-3">
        {FeatureRender}
    </div>
  );
};

export default Features;
