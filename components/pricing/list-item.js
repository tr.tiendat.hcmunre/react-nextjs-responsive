import React from 'react';
import Image from 'next/image';
import PricingListItem from "/public/assets/pricing-list-item.svg";

export default function ListItem({service}) {
    return (
        <div className="col-md-4 col-lg-3 pb-4">
            <div className={`card h-100 border-0 rounded-3 secondary-bg`}>
                <div className="card-body">
                <h4 className="card-title fw-bold">
                    {service?.type}
                    <span className="badge primary-bg ms-1 font-13">{service?.tag}</span>
                </h4>
                <h4 className="fw-bold ps-3">
                    {service?.price}<small className="fs-6">.99$/month</small>
                </h4>
                <p className="card-text border-secondary border-bottom pb-3 ps-3">{service?.description}</p>
                <ul className={`list-unstyled pb-5 ps-3`}>
                {service?.numLorem.map((sub) => {
                    return (
                        <li key={sub}>
                            <Image
                                src={PricingListItem}
                                priority={true}
                                width={16}
                                height={16}
                                className=""
                                alt=""
                            ></Image>
                            <span className="font-13 position-absolute ps-2">Lorem ispum</span>
                        </li>
                    );
                })}
                </ul>
                <div className="position-absolute bottom-10 m-0 start-50 translate-middle">
                    <a href="#" className="btn btn-primary primary-bg border-0 fw-bold shadow">
                        BUY NOW
                    </a>
                </div>
                </div>
            </div>
        </div>
    )
}
