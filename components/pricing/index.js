import React from "react";
import ListItem from './list-item';

export default function index() {
    const services = [
        {
          id: 1,
          type: 'Personal',
          price: '9',
          description:
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam',
    
          numLorem: [1, 2, 3],
        },
        {
          id: 2,
          type: 'Bussiness',
          price: '29',
          tag: 'Most Popular',
          description:
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam',
    
          numLorem: [1, 2, 3, 4, 5],
        },
        {
          id: 3,
          type: 'Enterpise',
          price: '59',
          description:
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam',
    
          numLorem: [1, 2, 3, 4, 5, 6, 7],
        },
    ];
    
    return (
        <div className="row justify-content-evenly">
            {services.map((item) => {
                return <ListItem key={item.id} service={item} />
            })}
        </div>
    );
}
