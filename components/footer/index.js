import React from 'react';
import Image from 'next/image';
import SkypeLogo from '/public/assets/skype.svg';
import FacbookLogo from '/public/assets/facebook.svg';
import InsLogo from '/public/assets/ins.svg';
import logo from '/public/assets/logo-png.png';

export default function index() {
    return (
        <div className="container-fluid secondary-bg">
            <footer className="row pt-3 pb-3">
                <div className="col-md-4 col-lg-4 col-sm-12 text-center align-self-center">
                    <Image
                        src={logo}
                        width={150}
                        height={50}
                        objectFit="contain"
                        // layout="responsive"
                        alt="logo"
                    />
                </div>
                <div className="col-md-4 col-lg-4 col-sm-12 text-center align-self-center">
                    <span className="font-13"> © 2021 Vizion All Rights Reserved. </span>
                </div>
                <div className="col-md-4 col-lg-4 col-sm-12 text-center align-self-center">
                    <div className="d-flex justify-content-center">
                        <div className="p-2 d-flex align-self-center">
                            <Image className="primary-bg p-1 rounded-circle" width={26} height={26} src={SkypeLogo} alt="" />
                        </div>
                        <div className="p-2 d-flex align-self-center">
                            <Image className="primary-bg p-1 rounded-circle" width={26} height={26} src={FacbookLogo} alt="" />
                        </div>
                        <div className="p-2 d-flex align-self-center">
                            <Image className="primary-bg p-1 rounded-circle" width={26} height={26} src={InsLogo} alt="" />
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    )
}
